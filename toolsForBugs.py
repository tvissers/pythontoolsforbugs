#!python

# tools for bugs is written to get started with reading files containing found positions and orientations in frames and trajectories

import numpy as np
from numpy import *
import matplotlib.pyplot as plt
from matplotlib import *
from matplotlib.pyplot import *

def readTrackingFile(filename, minimumTrackLength=0.0):
        """Reads a file containing trajectories produced by trackRods2Dt (using a positions file obtained with findRods2Dt)
	   Reads 'new file format'. See code for details on file format.

            Parameters:
                filename - input file
		minimumTrackLength - to only read trajectories from file that are longer than this value
                
            Returns:
		listForAllTrajectories - list containing all trajectories
		numberOfFrames - number of frames in original dataset
        """

	FILE = open(filename,"r") # open file as "read"
	firstLine=FILE.readline()
	coor=firstLine.split(' ') # split into two parts
        numberOfTracks = int(coor[0][1:])
        numberOfFrames = int(coor[1][1:]) # casts part (from 1 to end) of the string firstLine to an integer number

        print 'Number of tracks: ', numberOfTracks # plots the number of trajectories
        print 'Number of frames: ', numberOfFrames # plots the number of frames
	listForAllTrajectories = []
        PROPERTIES = []
	numberOfSelectedTracks = 0;

        for i in range(numberOfTracks):
		headerLine=FILE.readline()
                coor=headerLine.split(' ') # split into two parts
                trackNumber=int(coor[0][1:])
                numberOfFramesInThisTrack=int(coor[1][1:])
		
		M=np.zeros((numberOfFramesInThisTrack,12))
                for j in range(numberOfFramesInThisTrack):
                        coordinates=FILE.readline()
                        coor=coordinates.split(' ')
                        thistime = float(coor[0])
			someid = float(coor[1])
                        xvalue = float(coor[2])
                        yvalue = float(coor[3])
                        xor = float(coor[4])
                        yor = float(coor[5])
                        L = float(coor[6])
                        fitpar1 = float(coor[7])
                        fitpar2 = float(coor[8])
                        fitpar3 = float(coor[9])
                        width = float(coor[10])
                        fitpar4 = float(coor[11])
			V=array([xor, yor])
			n=np.linalg.norm(V)
			L=n*2.0
			if (n != 0.0):
				V=V/n
	
                        M[j,0] = thistime # number of frames since start of the video
                        M[j,1] = someid # original id number of bacterium in the frame
                        M[j,2] = xvalue # x position in pixels obtained with findRods2Dt
                        M[j,3] = yvalue # y position in pixels obtained with findRods2Dt
                        M[j,4] = V[0] # normalised orientation x-component obtained with findRods2Dt
                        M[j,5] = V[1] # normalised orientation y-component obtained with findRods2Dt
			M[j,6] = L     # length from fit obtained with findRods2Dt (in pixels) 
			M[j,7] = fitpar1 # aspect ratio lengthfromfit /widthfromfit obtained with findRods2Dt
			M[j,8] = fitpar2 # (lengthfromfit-widhtfromfit)/widthfromfit obtained with findRods2Dt
			M[j,9] = fitpar3 # aspect ratio lengthfromfit / estimated width from input file obtained with findRods2Dt
			M[j,10] = width  # width from fit (in pixels) obtained with findRods2Dt
			M[j,11] = fitpar4  # sqrt(largesteigenvalue/smallesteigenvalue) obtained with findRods2Dt

		if (numberOfFramesInThisTrack > minimumTrackLength):
                	listForAllTrajectories.append(M) # add positions for this frame to the list.
			numberOfSelectedTracks += 1

	return listForAllTrajectories, numberOfFrames

def readCoordinateFile(filename):
	"""Reads a coordinate file produced by findRods2Dt. See code for details on file format.
	    
	    Parameters:
		filename - input file
		
	    Returns:
		positionsForAllFrames - list containing coordinates per frame
		propertiesForAllFrames - properties of each frame	
	"""

    	FILE = open(filename,"r") # open file as "read"

	firstLine=FILE.readline()
	numberOfFrames = int(firstLine[1:])  # casts part (from 1 to end) of the string firstLine to an integer number
	print 'Number of frames:', numberOfFrames # plots the number of frames
	
	positionsForAllFrames = []
	propertiesForAllFrames = []
	for i in range(numberOfFrames): # loops through all the frames 	
		headerLine=FILE.readline()
		coor=headerLine.split(' ') # split into two parts
		numberOfTheFrame=int(coor[0][1:])
		numberOfBacteriaInTheFrame=int(coor[1][1:])
		dimensionLine=FILE.readline()
		coor=dimensionLine.split(' ') # split into multiple parts
		xmax=float(coor[0]) 
		ymax=float(coor[1])
		M=np.zeros((numberOfBacteriaInTheFrame,12))
		for j in range(numberOfBacteriaInTheFrame):		
			coordinates=FILE.readline()
			coor=coordinates.split(' ')
			xvalue = float(coor[0])
			yvalue = float(coor[1])
			zvalue = float(coor[2])
			xpol = float(coor[3])
			ypol = float(coor[4])
			zpol = float(coor[5])
			fitpar1 = float(coor[6])
			fitpar2 = float(coor[7])
			fitpar3 = float(coor[8])
			width = float(coor[9])
			length = float(coor[10])
			fitpar4 = float(coor[11])
			M[j,0] = xvalue					# x-coordinate COM
			M[j,1] = yvalue					# y-coordinate COM
			M[j,2] = zvalue					# z-coordinate (0 for 2D data set)
			M[j,3] = xpol					# x-coordinate of one of the poles obtained with findRods2Dt
			M[j,4] = ypol					# y-coordinate of one of the poles obtained with findRods2Dt
			M[j,5] = zpol					# z-coordinate of one of the poles (0 for 2D data set)
			M[j,6] = fitpar1				# aspect ratio lengthfromfit/widthfromfit obtained with findRods2Dt
			M[j,7] = fitpar2				# (lengthfromfit-widhtfromfit)/widthfromfit obtained with findRods2Dt
			M[j,8] = fitpar3				# aspect ratio lengthfromfit / estimated width from input file obtained with findRods2Dt
			M[j,9] = width					# width of the rod from fit obtained with findRods2Dt
			M[j,10] = length				# length of the rod from fit obtained with findRods2Dt
			M[j,11] = fitpar4				# sqrt(largesteigenvalue/smallesteigenvalue) obtained with findRods2Dt
		
		positionsForAllFrames.append(M)
		S = np.shape(M)		
		
		propertiesForAllFrames.append([xmax, ymax, S[0]]) # max dimension x (pixels), max dimension y (pixels), number of bacteria in frame
	
    	FILE.close()
	return positionsForAllFrames, propertiesForAllFrames

