Python 2.7 code to read positions and trajectory files:

* toolsForBugs.py contains functions to read positions and trajectories.

Also two example files:

* positions.dat file containing positions and orientations of rod-shaped objects. File format is summarised below.
* tracks.dat file containing trajectories. File format is summarised below.



--------------------------------------------------------------------------------

About: positions.dat - file format

--- text -----------------------------------------------------------------------
&NUMBEROFFRAMES
&FRAMENO &NUMBEROFPARTICLESINTHISFRAME
FIELD_PIXELS_X FIELD_PIXELS_Y VERSION_ID
COM_X COM_Y COM_Z POLE_X POLE_Y POLE_Z FITPAR1 FITPAR2 FITPAR3 WIDTH LENGTH FITPAR4
COM_X COM_Y COM_Z POLE_X POLE_Y POLE_Z FITPAR1 FITPAR2 FITPAR3 WIDTH LENGTH FITPAR4
COM_X COM_Y COM_Z POLE_X POLE_Y POLE_Z FITPAR1 FITPAR2 FITPAR3 WIDTH LENGTH FITPAR4
..
..
..
COM_X COM_Y COM_Z POLE_X POLE_Y POLE_Z FITPAR1 FITPAR2 FITPAR3 WIDTH LENGTH FITPAR4
&FRAMENO &NUMBEROFPARTICLESINTHISFRAME
FIELD_PIXELS_X FIELD_PIXELS_Y VERSION_ID
..
..
--------------------------------------------------------------------------------

Legend:

--- text -----------------------------------------------------------------------
NUMBEROFFRAMES  :       total number of frames
--------------------------------------------------------------------------------

For each frame, the following info is given:

--- text -----------------------------------------------------------------------
FRAMENO         :       frame number, starts with 0
FIELD_PIXELS_X  :       max pixel value in x-direction
FIELD_PIXELS_Y  :       max pixel value in y-direction
VERSION_ID      :       Some file ID, typically set to -10
--------------------------------------------------------------------------------

For each found rod-shaped particle, the following info is given on each line:

--- text -----------------------------------------------------------------------
COM_X           :       x-coordinate COM
COM_Y           :       y-coordinate COM
COM_Z           :       z-coordinate (0 for 2D data set)
POLE_X          :       x-coordinate of one of the poles
POLE_Y          :       y-coordinate of one of the poles
POLE_Z          :       z-coordinate of one of the poles (0 for 2D data set)
FITPAR1         :       aspect ratio lengthfromfit/widthfromfit
FITPAR2         :       (lengthfromfit-widhtfromfit)/widthfromfit
FITPAR3         :       aspect ratio lengthfromfit / estimated width from input file
WIDTH           :       width of the rod from fit
LENGTH          :       length of the rod from fit
FITPAR4         :       sqrt(largesteigenvalue/smallesteigenvalue)
--------------------------------------------------------------------------------







--------------------------------------------------------------------------------

About: tracks.dat - file format

This file contains trajectories constructed from the positions in consecutive frames originally obtained with findRods2Dt.

--- text -----------------------------------------------------------------------
&NUMBEROFTRAJECTORIES &NUMBEROFFRAMES VERSION_ID
&TRAJECTORYNO &TRAJECTORYDURATION
FRAMENO PARTICLEID X_COM Y_COM X_OR Y_OR LENGTH FITPAR1 FITPAR2 FITPAR3 WIDTH FITPAR4
FRAMENO PARTICLEID X_COM Y_COM X_OR Y_OR LENGTH FITPAR1 FITPAR2 FITPAR3 WIDTH FITPAR4
FRAMENO PARTICLEID X_COM Y_COM X_OR Y_OR LENGTH FITPAR1 FITPAR2 FITPAR3 WIDTH FITPAR4
..
..
..
&TRAJECTORYNO &TRAJECTORYDURATION
FRAMENO PARTICLEID X_COM Y_COM X_OR Y_OR LENGTH FITPAR1 FITPAR2 FITPAR3 WIDTH FITPAR4
..
..
--------------------------------------------------------------------------------

Legend:

--- text -----------------------------------------------------------------------
NUMBEROFTRAJECTORIES    :       total number of trajectories
NUMBEROFFRAMES          :       total number of frames
VERSION_ID              :       negative number is new (default) format, positive number is old format
--------------------------------------------------------------------------------

For each trajectory is written:

--- text -----------------------------------------------------------------------
TRAJECTORYNO            :       ID of the trajectory, starts at 0
TRAJECTORYDURATION      :       Duration (in frames) of the trajectory
--------------------------------------------------------------------------------

For each frame in the trajectory is given:

--- text -----------------------------------------------------------------------
FRAMENO         :       frameno since start of the video
PARTICLEID      :       original ID number of rod-shaped object in the frame
X_COM           :       x position in pixels
Y_COM           :       y position in pixels
X_OR            :       x-component of vector (not normalised) pointing from COM to POLE
Y_OR            :       y-component of vector (not normalised) pointing from COM to POLE
LENGTH          :       length (in pixels)
FITPAR1         :       aspect ratio lengthfromfit /widthfromfit obtained with findRods2Dt
FITPAR2         :       (lengthfromfit-widhtfromfit)/widthfromfit obtained with findRods2Dt
FITPAR3         :       aspect ratio lengthfromfit / estimated width from input file obtained with findRods2Dt
WIDTH           :       width (in pixels)
FITPAR4         :       sqrt(largesteigenvalue/smallesteigenvalue) obtained with findRods2Dt
--------------------------------------------------------------------------------




